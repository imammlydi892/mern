const express = require('express');
const router = express.Router();
const {body}=require('express-validator');
const authorizationToken = require('../config/authorizationToken')


const CategoryController = require('../controllers/categoryController')


router.post('/save', CategoryController.createCategory);
router.get('/get', CategoryController.findCategory);

module.exports = router;