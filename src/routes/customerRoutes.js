const express = require('express');
const router = express.Router();
const authorizationToken = require('../config/authorizationToken')


const CustomerController = require('../controllers/customerController')

router.post('/create_customer', CustomerController.createCustomer);
router.get('/all_customers', CustomerController.getAllCustomers);
router.delete('/delete_customer/:customerId', CustomerController.deletCustomer);
router.get('/delete_customer/:customerId',authorizationToken, CustomerController.findCutomerById);

module.exports = router;