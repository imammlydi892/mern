const express = require('express');
const router = express.Router();
const {body}=require('express-validator');
const authorizationToken = require('../config/authorizationToken');

const RolesController = require('../controllers/rolesController')


router.post('/save', RolesController.createController);
router.get('/all', RolesController.allRoles);

module.exports = router;