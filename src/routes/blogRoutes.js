const express = require('express');
const router = express.Router();
const {body}=require('express-validator');

const blogController= require('../controllers/blogController');

//IMPORT TOKEN AUTHORIZATION
const authorizationToken = require('../config/authorizationToken');
const authRole = require('../config/authRole')


// ADD ARRAY FOR VALIDATOR
router.post('/post', [
    body('title').isLength({min:5}).withMessage('minimal 5 karakter'), 
    body('body').isLength({min:5}).withMessage('minimal 5 karakter')] , 
    blogController.create);


router.get('/posts',authorizationToken,authRole.authRoleAdmin,
    blogController.getAllBlogPost
  );
  
router.get('/post/:postId', blogController.getBlogPostById);

router.put('/post/:postId',[
  body('title').isLength({min:5}).withMessage('minimal 5 karakter'), 
  body('body').isLength({min:5}).withMessage('minimal 5 karakter')] ,
  blogController.updateBlogPost);

router.delete('/post/:postId', blogController.deleteBlogPost)

module.exports = router;