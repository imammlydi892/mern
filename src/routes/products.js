const express = require('express');
const router = express.Router();


const productsController = require('../controllers/products');

router.get('/products',productsController.getProduct)

router.post('/product', productsController.createProduct)

router.put('/products', productsController.putProduct)

router.delete('/products', productsController.deleteProduct)

module.exports = router;