const express =  require('express');
const {body} = require('express-validator');
const router = express.Router();


const authController = require('../controllers/authController')
const authUser = require('../controllers/authUserController')

router.post('/reg', [
    body('name').isLength({min:5}).withMessage('minimal 5 karakter'),
    body('email').isLength({min:5}).withMessage('minimal 5 karakter'),
    body('password').isLength({min:6}).withMessage('minimal 5 karakter')
] , authController.register);

router.post('/register/:role_id', authUser.autUserControllerRegister);

router.post('/login', authUser.autUserControllerLogin);

router.get('/get', authUser.allUsers)

module.exports = router;