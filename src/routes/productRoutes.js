const express = require('express');
const router = express.Router();
const {body}=require('express-validator');
const req = require('express/lib/request');
const authorizationToken = require('../config/authorizationToken');
const ROLE = require('../models/rolesUserModel')

const ProductControllers = require('../controllers/productContoller');
const { send } = require('express/lib/response');
const ADMIN = () =>{
    ROLE.find()
     .then(result =>{
         res.status(200).json({
             data: result[0]._id
         })
     })
     .catch(err => {
         next(err)
     })
}


router.post('/create_product/:category_id',[ body('description').isLength({min:5}).withMessage('minimal 5 karakter')],ProductControllers.createProduct);
router.get('/all_product', ProductControllers.findAllProducts);
router.get('/findOne_product/:product_id',authorizationToken, ProductControllers.findByIdProduct);
router.delete('/delete_product/:product_id',authorizationToken, ProductControllers.deleteProduct);
router.post('/addCategory/:category_id', ProductControllers.addCategory)
router.get('/roles',  (req,res,next) => {
     
     ROLE.find()
     .then(result =>{
         res.status(200).json({
             data: result[0]._id
         })
     })
     .catch(err => {
         next(err)
     })
})


module.exports = router;