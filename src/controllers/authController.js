const {validationResult} = require('express-validator')

exports.register =  (req,res,next) => {

    // Validasi Error
    const errorValidation = validationResult(req);

    if(!errorValidation.isEmpty()){
        const err = new Error('Input Value tidak sesuai');
        err.errorStatus = 400;
        err.data = errorValidation.array();
        throw err;
    }

    const name = req.body.name;
    const email = req.body.email;
    const password = req.body.password;

    const result = {
            message:'Register Succesfull',
            data:{
                uid:1,
                name: name,
                email:email,
                passwors: password
            }
    }
    res.status(201).json(result)
}