const CustomerModel = require ('../models/customerModel');


exports.createCustomer = (req,res,next) =>{

    const fullname = req.body.fullname;
    const email = req.body.email;
    const legal_id = req.body.legal_id;
    const company = req.body.company;
    const tax_id = req.body.tax_id;

    const CreateNewCostumer = new CustomerModel({
        fullname:fullname,
        email:email,
        legal_id:legal_id,
        company:company,
        tax_id:tax_id
    });

    CreateNewCostumer.save()
    .then(result =>{
        const results = {
            message:'Create Customer Successfull',
            data:result
        }
        res.status(201).json(results)    
    })
    .catch(err =>{
        console.log('errornya..  ',err)
    });
}

exports.getAllCustomers = (req,res,next) =>{

    CustomerModel.find()
    .then(result =>{
        const results = {
            message:'Data seluruh Customersr',
            data:result
        }
        res.status(201).json(results)    
    })
    .catch(err =>{
        console.log('errornya..  ',err)
    });

}

exports.deletCustomer = (req,res,next) =>{

    const customerId = req.params.customerId;
    CustomerModel.findById(customerId)
    .then(customer =>{
        if(!customer){
            const error = new Error('Cutomer tidak di temukan');
                error.errorStatus = 404;
                throw error;
        }
        return CustomerModel.findByIdAndRemove(customerId);
    })
    .then(result =>{
        res.status(200).json({
            message:'data berhasil di Delete',
            data:result
        })
    })
    .catch(err =>{
        next(err);
    })
}

exports.findCutomerById = (req,res,next) =>{
    const customerId = req.params.customerId;
    CustomerModel.findById(customerId)
    .then(customer =>{
        if(!customer){
            const error = new Error('Cutomer tidak di temukan');
                error.errorStatus = 404;
                throw error;
        }
        res.status(200).json({
            message:'data berhasil di temukan',
            data:customer
        })
    })
    .catch(err =>{
        next(err);
    })
}