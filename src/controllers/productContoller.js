const {validationResult} = require('express-validator');
const ProductModel = require('../models/productModel');
const CategoryModel = require('../models/categoryModel')


exports.createProduct = (req,res,next) =>{
    const errorValidation= validationResult(req);

    if(!errorValidation.isEmpty()){
        const err = new Error('Input Value tidak sesuai ');
        err.errorStatus = 400;
        err.data = errorValidation.array();
        throw err;  
    }


    const product_name= req.body.product_name;
    const description= req.body.description;
    const price= req.body.price;
    const review= req.body.review;
    const condition= req.body.condition;
    const size= req.body.size;
    const spesification= req.body.spesification;
    const weight= req.body.weight;
    const CategoryModel= req.params.CategoryModel._id

    const PreateProduct = new ProductModel({
    product_name: product_name,
    description: description,
    price: price,
    review:review,
    condition: condition,
    size: size,
    spesification: spesification,
    weight: weight,
    category:CategoryModel._id
    });


    PreateProduct.save()
    .then(result =>{
        const results = {
            message:'Create Price Successfull',
            data:result
        }
        res.status(201).json(results)    
    })
    .catch(err =>{
        console.log('errornya..  ',err)
    });

}

exports.findAllProducts = (req,res,next) =>{
    
    ProductModel.find()
    .then(result => {
        res.status(200).json({
            message:'Data semua Product',
            data:result
        })
    })
    .catch(err => {
        next(err)
    })
}

exports.deleteProduct = (req,res,next) =>{

    const product_id = req.params.product_id;
    ProductModel.findById(product_id)
    .then(product =>{
        if(!product){
            const error = new Error('Cutomer tidak di temukan');
                error.errorStatus = 404;
                throw error;
        }

        return ProductModel.findByIdAndRemove(product_id);
    })
    .then(result =>{
        res.status(200).json({
            message:`data dengan ${product_id} berhasil di hapus`,
            data:result
        })
    })
    .catch(err => {
        next(err);
    })
}

exports.findByIdProduct = (req,res,next) =>{
    const product_id = req.params.product_id;
    ProductModel.findById(product_id)
    .then(product =>{
        if(!product){
            const error = new Error('Cutomer tidak di temukan');
                error.errorStatus = 404;
                throw error;
        }

        res.status(200).json({
            message:`data dengan ${product_id} berhasil di hapus`,
            data:product
        })
    })
    .catch(err => {
        next(err);
    })
}


// add Category

exports.addCategory =async (req,res,next) =>{
   try { const errorValidation= validationResult(req);

    if(!errorValidation.isEmpty()){
        const err = new Error('Input Value tidak sesuai ');
        err.errorStatus = 400;
        err.data = errorValidation.array();
        throw err;  
    }

    const product_name= req.body.product_name;
    const description= req.body.description;
    const price= req.body.price;
    const review= req.body.review;
    const condition= req.body.condition;
    const size= req.body.size;
    const spesification= req.body.spesification;
    const weight= req.body.weight;
    const category_id= req.params.category_id

    const createProduct = new ProductModel({
    product_name: product_name,
    description: description,
    price: price,
    review:review,
    condition: condition,
    size: size,
    spesification: spesification,
    weight: weight,
    category:category_id
    });

    await createProduct.save();

    const category = await CategoryModel.findById({_id:createProduct.category})
    category.products.push(createProduct)
    await category.save()

    res.status(200).json({
        message:'success',
        data:category
    })

}catch(err){
    res.status(400).json({message:'gagal'})
}
} 
