const CategoryModel = require('../models/categoryModel');
const {validationResult} = require('express-validator');
const { result } = require('@hapi/joi/lib/base');


exports.createCategory = (req,res,next) =>{

    const errorValidation= validationResult(req);
    if(!errorValidation.isEmpty()){
        const err = new Error('Input Value tidak sesuai ');
        err.errorStatus = 400;
        err.data = errorValidation.array();
        throw err;  
    }

    const name = req.body.name;
    // const {product_id} = req.body.product_id

    const NewCategory = new CategoryModel({
        name : name
        // products:[{
        //     productModel:product_id
        // }]
    });

    NewCategory.save()
    .then(result =>{
        res.status(200).json({
            message:'create category successfully',
            data:result
        })
    })
    .catch(err =>{
        next(err)
    })

}

exports.findCategory = (req, res, next) =>{

    CategoryModel.find()
    .then(results => {
        res.status(200).json({
            message:'Data all Category',
            data: results
        })
    })
    .catch(err => {
        next(err)
    })
}