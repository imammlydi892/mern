
// IMPORT VALIDATION
const {validationResult} = require('express-validator');
const BlogPost = require('../models/blogModel')
const path =require('path');

// REMOVE FILE SYSTEM(IMAGE)
const fs = require('fs');
const { count } = require('console');
 

    exports.create = (req,res,next) =>{

        const errorValidation= validationResult(req);

        if(!errorValidation.isEmpty()){
            const err = new Error('Input Value tidak sesuai ');
            err.errorStatus = 400;
            err.data = errorValidation.array();
            throw err;  
        }
        if(!req.file){
            const err = new Error('Image Harus di upload');
            err.errorStatus = 422;
            err.data = errorValidation.array();
            throw err;  
        }

    
        
        const title = req.body.title;
        const image = req.file.path;
        const body = req.body.body;

        const Posting = new BlogPost({
            title:title,
            body:body,
            image:image,
            author:{
                uid:1,
                name:'Imam Mulydi'
            }
        })

        Posting.save()
        .then(result =>{
            const results = {
                message:'Create Blog Blog Successfull',
                data:result
            }
            res.status(201).json(results)    
        })
        .catch(err =>{
            console.log('errornya..  ',err)
        });
  
    }




    

    exports.getAllBlogPost= (req,res,next) =>{
        const currentPage = req.query.page || 1;
        const perPage=req.query.perPage || 5;
        let totalItems;

        // HITUNG ALL DATA
        BlogPost.find()
        .countDocuments()
        .then(count => {
            totalItems=count;
            return BlogPost.find()
            .skip((currentPage-1)*perPage)
            .limit(perPage)
        })
        .then(result => {
            res.status(200).json({
                message:'Data BlogPost berhasil di panggil',
                data:result,
                total_data:totalItems,
                per_page:perPage,
                current_page:currentPage
            })
        })
        .catch(err =>{
            next(err);
            console.log('errornya..  ',err)
        })

        // BlogPost.find()
        // .then(result => {
        //     res.status(200).json({
        //         message:'Data BlogPost berhasil di panggil',
        //         data:result
        //     })
        // })
        // .catch(err => {
        //     next(err);
        //     console.log('errornya..  ',err)
        // })
    }


    exports.getBlogPostById = (req,res,next) => {
        const postId= req.params.postId
        BlogPost.findById(postId)
        .then(result => {
            if(!result){
                const error = new Error('Blog Post tidak di temukan');
                error.errorStatus = 404;
                throw error;
            }
            res.status(200).json({
                message:'Data BlogPost berhasil di panggil',
                data:result
            })
        })
        .catch(err => {
            next(err)
        })
    }

    exports.updateBlogPost= (req,res,next) => {
        const errorValidation= validationResult(req);

        if(!errorValidation.isEmpty()){
            const err = new Error('Input Value tidak sesuai ');
            err.errorStatus = 400;
            err.data = errorValidation.array();
            throw err;  
        }
        if(!req.file){
            const err = new Error('Image Harus di upload');
            err.errorStatus = 422;
            err.data = errorValidation.array();
            throw err;  
        }

        const title = req.body.title;
        const image = req.file.path;
        const body = req.body.body;
        const postId= req.params.postId

        BlogPost.findById(postId)
        .then(post => {
            if(!post){
                const error = new Error('Blog Post tidak di temukan');
                error.errorStatus = 404;
                throw error;
            }
            post.title=title;
            post.image=image;
            post.body=body;
            return post.save(); 
            
        })
        .then(result => {
            res.status(200).json({
                message:'Data BlogPost berhasil di panggil',
                data:result
            })
        })
        .catch(err => {
            next(err)
        })
    }

    exports.deleteBlogPost= (req,res,next) =>{

        const postId=req.params.postId
        BlogPost.findById(postId)
        .then(post =>{

            if(!post){
                const error = new Error('Blog Post tidak di temukan');
                error.errorStatus = 404;
                throw error;
            }

            removeImage(post.image);
            return BlogPost.findByIdAndRemove(postId);
            
        })
        .then(result => {
            res.status(200).json({
                message:'delete blog Berhasil',
                data:result
            });
        })
        .catch(err =>{
            next(err)
        })
    }


    const removeImage = (filePath) =>{
        console.log('filepath', filePath);
        console.log('dir_name',__dirname);

        filePath = path.join(__dirname,'../..' ,filePath);
        fs.unlink(filePath, err => console.log('error hapus ...', err));
    }