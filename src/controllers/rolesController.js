const RolesModel = require('../models/rolesUserModel');
const {validationResult} = require('express-validator');


exports.createController = (req,res,next) =>{
    const errorValidation= validationResult(req);
    if(!errorValidation.isEmpty()){
        const err = new Error('Input Value tidak sesuai ');
        err.errorStatus = 400;
        err.data = errorValidation.array();
        throw err;  
    }

    const name = req.body.name;

    const CreateRoles = new RolesModel({
        name : name
    })

    CreateRoles.save()
    .then(result => {
        res.status(200).json({
            message:`roles dengan nama ${name} telah di buat`,
            data: result
        })
    })
    .catch(err =>{
        next(err)
    })
}

exports.allRoles = (req,res,next) =>{
    RolesModel.find()
    .then(result =>{
        res.status(200).json({
            message:'success all roles',
            data: result
        })
    })
    .catch(err => {{
        next(err)
    }})
}