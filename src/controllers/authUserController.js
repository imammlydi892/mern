const {validationResult} = require('express-validator');
const authUser = require('../models/authUserModel');
const bcrypt = require('bcryptjs');
const roles = require('../models/rolesUserModel')

//import VALIDATION
const {registerValidation} = require('../config/validation');
const res = require('express/lib/response');
const req = require('express/lib/request');

//IMPORT JWT
const jwt = require('jsonwebtoken');
const rolesUserModel = require('../models/rolesUserModel');


exports.autUserControllerRegister = async (req,res,next) =>{

    //VALIDATION WITH JOI
    const {error} = registerValidation(req.body);
    
    // res.send(error.details[0].message)
    if(error)return res.status(400).json({
        status:res.statusCode,
            message:error.details[0].message
    })

    //CHECK USER EXIST
    const emailExist = await authUser.findOne({email:req.body.email})
    if(emailExist) return res.status(400).json({
        status:res.statusCode,
            message:"Email sudah terdaftar !!!"
    })

    //HASH / BCRYPT PASSWORD
    const salt = await bcrypt.genSalt(10);
    const hashPassword =await  bcrypt.hash(req.body.password, salt);
    const rr = req.params.role_id

    const User = new authUser({
        name:req.body.name,
        email:req.body.email,
        password:hashPassword,
        // role: rr
    })

    // User.save()
    // .then(result =>{
    //     const results = {
    //         message:'Create Blog Blog Successfull',
    //         data:result
    //     }
    //     res.status(201).json(results);
    // })
    // .catch(err =>{
    //     console.log('errornya..  ',err)
    // })

    try{
        const roless = await roles.findById({_id:rr})
        User.roles.push(roless)
        const saveUser = await User.save();
        const results = {
                    message:'Create Blog Blog Successfull',
                    data:saveUser
                }
        res.status(201).json(results);
    }catch(err){
        res.status(400).json({
            status:res.statusCode,
            message:"gagal membuat user baru",err
        })
    }
}


exports.autUserControllerLogin =async (req,res,next) =>{


    //CHECK USER EXIST
    const user = await authUser.findOne({email:req.body.email})
    if(!user) return res.status(400).json({
        status:res.statusCode,
            message:"Email anda salah !!!"
    })

    const validPassword =await  bcrypt.compare(req.body.password, user.password)
    if(!validPassword) return  res.status(400).json({
        status:res.statusCode,
            message:"Password anda salah  !!!"
    })

    // res.send('Berhasil Login');

    //JWT TOKEN
    const SCRET_KEY = "dfjkghriueghutrjhjtn"
    const token = jwt.sign({_id: user._id, role: user.roles}, SCRET_KEY, { expiresIn: 200 });
    res.header('auth_token',token).json({
        token:token
    });

}

exports.allUsers = (req,res,next) => {
    authUser.find()
    .then(result =>{
        res.status(200).json({
            message: 'success All Users',
            data: result
        })
    })
}