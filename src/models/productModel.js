const text = require('body-parser/lib/types/text');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const productModel = new Schema({
    product_name:{
        type: String,
        required:true
    },
    description:{
        type: String,
        required:true
    },
    price:{
        type: Number,
        required:true
    },
    review:{
        type: String,
        required:true
    },
    condition:{
        type: String,
        required:true
    },
    size:{
        type: String,
        required:true
    },
    spesification:{
        type: String,
        required:true
    },
    weight:{
        type: String,
        required:true
    },
    category:{
        type:Schema.Types.ObjectId,
        ref:'categoryModel'
    }
},{
    timestamps:true
});


module.exports = mongoose.model('productModel', productModel)