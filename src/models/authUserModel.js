const mongoose = require('mongoose');
const Schema = mongoose.Schema;


AuthUser = new Schema({
    name: {
        type: String,
        required:true,
        max:255
    },
    email : {
        type: String,
        required:true,
        max:100
    },
    password:{
        type: String,
        required:true,
        min:6,
        max:1024
    },
    createdAt:{
        type:Date,
        default:Date.now
    },
    roles: [
        {
            type: Schema.Types.ObjectId,
            ref: "rolesUserModel"
        }
    ]
})



module.exports = mongoose.model('authUser', AuthUser)