const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const categoryModel = new Schema({
    name:{
        type:String,
        required:true
    },
    products:[{
        type:Schema.Types.ObjectId,
        ref:'productModel'
    }]
},{
    timestamps:true
});


module.exports = mongoose.model('categoryModel',categoryModel)