const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const rolesUserModel = new Schema({
    name : {
        type: String,
        required:true
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: "authUser"
    }
},{
    timestamps:true
})


module.exports = mongoose.model('rolesUserModel', rolesUserModel)