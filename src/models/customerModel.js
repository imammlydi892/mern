const mongoose = require('mongoose');
const Schema =mongoose.Schema;


const customerModel = new Schema({
    fullname:{
        type: String,
        required:true
    },
    email :{
        type: String,
        required:true
    },
    legal_id:{
        type: String,
        required:true
    },
    company:{
        type: String,
        required:true
    },
    tax_id:{
        type: String,
        required:true
    }
},{
    timestamps:true
});

module.exports = mongoose.model('customerModel', customerModel)