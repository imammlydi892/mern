const mongoose = require('mongoose');
const Schema =mongoose.Schema;


const orderModel = new Schema({
    product_id:{
        type: Schema.Types.ObjectId,
        ref:'Product'
    },
    phone_number:{
        type: String,
        required:true
    },
    status_order:{
        type: String,
        required:true
    },
    user_log:{
        type: Schema.Types.ObjectId,
        ref:'User'
    }
},{
    timestamps:true
});

