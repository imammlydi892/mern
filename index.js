const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const multer = require('multer');
const path = require("path");   
var cors = require('cors')


const app=express();
const productRouters = require('./src/routes/products');
const authRotes = require('./src/routes/auth');
const blogRoutes = require('./src/routes/blogRoutes');
const productRoutes = require('./src/routes/productRoutes');
const customerRoutes = require('./src/routes/customerRoutes');
const categoryRoutes = require('./src/routes/categoryRoutes');
const rolesRoutes = require('./src/routes/rolesRoutes');


//MONGODB CLIENT
const MongoClient = require('mongodb').MongoClient;
const url = 'mongodb://127.0.0.1:27017/blog'


// FORMAT -MULTER- UPLOAD IMAGE
const fileStorage = multer.diskStorage({
    destination: (req,file,cb) => {
        cb(null, 'images');
    },
    filename: (req,file,cb) =>{
        cb(null,new Date().getTime()+'-'+ file.originalname);
    }
});

const fileFilter = (req,file,cb) =>{
    if(file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg'){
        cb(null,true);
    }else{
        cb(null,false);
    }
}

// MIDDLEWARE
app.use(bodyParser.json());
// STATIC GET URL IMAGES
app.use('/images', express.static(path.join(__dirname,'images')))
app.use(multer({storage:fileStorage, fileFilter:fileFilter}).single('image'));

//CORS POLICY
app.use((req,res,next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Method', 'GET,POST,PUT,DELETE,OPTION');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    // res.setHeader('Access-Control-Request-Headers', 'Content-Type, Authorization');
    next();
})

app.use(cors());

app.use('/v1/customer', productRouters);
app.use('/v1/auth',authRotes);
app.use('/v1/blog',blogRoutes);
app.use('/v1/product',productRoutes);
app.use('/v1/customer',customerRoutes);
app.use('/v1/category', categoryRoutes);
app.use('/v1/roles', rolesRoutes);

// MIDDELWARE GET DEFAULT ERROR
app.use((error,req,res,next) =>{
    const status = error.errorStatus || 500;
    const message = error.message;
    const data=error.data;

    res.status(status).json({message:message, data:data})
})

// MONGODB CONNECTION
mongoose.connect(url,{ useNewUrlParser: true })
    .then(() => {
        app.listen(4000, () => console.log('connection succes'));
    })
    .catch(err => console.log("errornya ",err))

//ONLINE URL
const ver04 = "mongodb+srv://imam:1xuur8PJiOnn1laU@cluster0.egzit.mongodb.net/Blog?retryWrites=true&w=majority"
const ver22 = "mongodb://imam:1xuur8PJiOnn1laU@cluster0-shard-00-00.egzit.mongodb.net:27017,cluster0-shard-00-01.egzit.mongodb.net:27017,cluster0-shard-00-02.egzit.mongodb.net:27017/Blog?ssl=true&replicaSet=atlas-1wohom-shard-0&authSource=admin&retryWrites=true&w=majority";

const verNew = "mongodb+srv://imam:1xuur8PJiOnn1laU@cluster0-shard-00-00.egzit.mongodb.net:27017,cluster0-shard-00-01.egzit.mongodb.net:27017,cluster0-shard-00-02.egzit.mongodb.net:27017/Blog?retryWrites=true&w=majority"




// MONGODB CLIENT OFFLINE
const dbName = 'blog'
let db
MongoClient.connect(url, { useNewUrlParser: true }, (err, client) => {
  if (err) return console.log(err)

  // Storing a reference to the database so you can use it later
  db = client.db(dbName)
  console.log(`Connected MongoDB: ${url}`)
  console.log(`Database: ${dbName}`)
})